<?php
    require_once('Animal.php');
    require_once('Frog.php');
    require_once('Ape.php');
    $object = new Animals('Sheep');

    echo "Name :". $object->name;
    echo "<br>";
    echo "Legs : " . $object->legs;
    echo "<br>";
    echo "cold blooded : " .$object->blood;
    echo "<br><br>";

    $object2 = new Frog('Buduk');
    echo "Name :" . $object2->name;
    echo "<br>";
    echo "Legs :" . $object2->legs;
    echo "<br>";
    echo "cold blooded :" . $object2->blood;
    echo "<br>";
    $object2->jump();
    echo "<br><br>";

    $object3 = new Ape('kera sakti');
    echo "Name :". $object3->name;
    echo "<br>";
    echo "Legs :". $object3->legs;
    echo "<br>";
    echo "cold blooded :" .$object3->blood;
    echo "<br>";
    $object3->yell();
    echo "<br>";
?>